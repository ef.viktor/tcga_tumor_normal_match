require(clusterProfiler)
require(ggplot2)

genes <- scan("out/experimental/final_cnrs_pos_corr.txt", what = character())

edo <- enrichGO(
  genes,
  'org.Hs.eg.db',
  keyType = "SYMBOL",
  ont = "BP",
  pvalueCutoff = 0.05,
  pAdjustMethod = "BH",
  qvalueCutoff = 0.2,
  minGSSize = 10,
  maxGSSize = 500,
  readable = FALSE
)

plot_1 <- dotplot(edo, showCategory = 30, font.size = 18) +
  ggtitle("Enrichment of positive correlated CNRs (Male vs. Female vs. Experimental)") +
  theme(
    plot.title = element_text(size = 20),
    legend.title = element_text(size = 16),
    legend.text = element_text(size = 16)
  )
ggsave("plots/final_cnrs_pos_corr.png", plot_1, width = 20, height = 14)
