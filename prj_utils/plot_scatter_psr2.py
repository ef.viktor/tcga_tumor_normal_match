import time
from typing import Optional, List

import pandas as pd
import scipy
import seaborn as sns
from matplotlib import pyplot as plt
from scipy.stats import spearmanr, pearsonr


def plot_scatter_psr2(
        df: pd.DataFrame, x: str, y: str,
        label_x: Optional[str] = None, label_y: Optional[str] = None,
        title: str = None,
        filename: str = None,
        labels: List[str] = None,
        show: bool = False,
        hue=None,
        ax=None
) -> None:
    if ax is None:
        _, ax = plt.subplots(figsize=(9, 6), dpi=200)
    sns.scatterplot(data=df, x=x, y=y, marker="v", hue=hue, ax=ax)

    a, b, r, p, err = scipy.stats.stats.linregress(df[x].values, df[y].values)
    linr = 'r2={:.2}; p-value={:.1e}'.format(r ** 2, p)
    limit_x_max = df[x].max()
    limit_x_min = df[x].min()
    ax.plot([limit_x_min, limit_x_max], [a * limit_x_min + b, a * limit_x_max + b], label=linr, color="grey")

    spear = 'Sp_r={:.2}; p-value={:.1e}'.format(
        spearmanr(df[x].values, df[y].values)[0],
        spearmanr(df[x].values, df[y].values)[1]
    )
    pear = 'P_r={:.2}; p-value={:.1e}'.format(
        pearsonr(df[x].values, df[y].values)[0],
        pearsonr(df[x].values, df[y].values)[1]
    )
    ax.scatter([], [], s=0, edgecolors='none', label=spear)
    ax.scatter([], [], s=0, edgecolors='none', label=pear)

    ax.set_xlabel(label_x or x)
    ax.set_ylabel(label_y or y)
    ax.legend(loc="lower right")

    if title:
        ax.title.set_text(title)

    if labels:
        for i, label_text in enumerate(labels):
            ax.annotate(label_text, (df[x].iloc[i] + 1, df[y].iloc[i]))

    if filename:
        filename_png = filename or f"outputs/{time.time()}-psr2.png"
        plt.savefig(filename_png, dpi=200, bbox_inches="tight")

    if show:
        plt.show()
