import os
import tempfile
from collections import defaultdict
from typing import Optional, List

import matplotlib_venn as vplt
import pandas as pd
import requests
from matplotlib import pyplot as plt
from sh import Command

ONCOBOX_TOOL = Command("oncoboxlib_calculate_scores")


def get_sample_tumor_status(sample_id: str) -> str:
    # https://docs.gdc.cancer.gov/Encyclopedia/pages/TCGA_Barcode/
    tumor_code = int(sample_id.split("-")[3][0:2])
    if 0 < tumor_code < 9:
        return "Tumor"
    elif 10 < tumor_code < 19:
        return "Normal"
    elif 20 < tumor_code < 29:
        return "Control"
    else:
        raise ValueError(f"Unknown tumor code '{tumor_code}'")


def get_patient_id(sample_id: str) -> str:
    return sample_id.split("-")[2]


def get_tcga_fields(sample_id: str, fields: List[str]) -> dict:
    json_header = {"Content-Type": "application/json"}
    case_endpoint = "https://api.gdc.cancer.gov/cases/"

    def get_filters(_submitter_id: str):
        return {
            "op": "=",
            "content": {
                "field": "submitter_id",
                "value": _submitter_id
            }
        }

    submitter_id = "-".join(sample_id.split("-")[:3])
    response = requests.post(
        case_endpoint,
        headers=json_header,
        json=dict(
            filters=get_filters(submitter_id),
            fields=",".join(fields),
            size=2000
        )
    )
    hits = response.json()["data"]["hits"]
    ret = defaultdict(dict)
    for field in fields:
        if hits:
            value = hits[0]
            for subfield in field.split("."):
                value = value.get(subfield)
                if value is None: break
            ret[field] = value
    return ret


def plot_deg_venn(df: pd.DataFrame, title: Optional[str] = None, out: Optional[str] = None):
    # setup variables
    regulations = ["up", "down"]
    sets = defaultdict(dict)
    subsets = dict()
    # extract upregulated and downregulated gene counts and subset intersections
    num_of_subsets = df.shape[1]
    assert num_of_subsets in [2, 3]
    for regulation in regulations:
        for sample in df:
            sets[regulation][sample] = set(df[df[sample] == {"up": 1, "down": -1}[regulation]].index)
        if num_of_subsets == 2:
            subsets[regulation] = vplt._venn2.compute_venn2_subsets(*sets[regulation].values())
        elif num_of_subsets == 3:
            subsets[regulation] = vplt._venn3.compute_venn3_subsets(*sets[regulation].values())
    # plot empty Venn of equal circles
    plt.subplots(figsize=(9, 6), dpi=200)
    if num_of_subsets == 2:
        v = vplt.venn2(subsets=[1] * 3, set_labels=df.columns)
    elif num_of_subsets == 3:
        v = vplt.venn3(subsets=[1] * 7, set_labels=df.columns)
    else:
        raise

    up_down_labels = list(zip(*subsets.values()))
    for curr_label, new_labels in zip(v.subset_labels, up_down_labels):
        curr_label.set_text(
            f"↑: {new_labels[0]}\n"
            f"↓: {new_labels[1]}"
        )
        curr_label.set_fontsize(16)

    for set_label in v.set_labels:
        set_label.set_fontsize(14)

    if title:
        plt.title(title)
    if out:
        plt.savefig(out, dpi=200, bbox_inches="tight")
    plt.show()


def calc_pals(data: pd.DataFrame, sample_id: str, norm_samples_ids: List[str]) -> pd.DataFrame:
    with tempfile.TemporaryDirectory() as tmp_dir:
        pals_in_df = data.filter([sample_id, *norm_samples_ids], axis=1)
        pals_in_df = pals_in_df.rename(columns={
            sample_id: "Tumor",
            **{x: f"Norm_{i}" for i, x in enumerate(norm_samples_ids)}
        })
        pals_in_df_path = os.path.join(tmp_dir, "pals_in.tsv")
        pals_in_df.to_csv(pals_in_df_path, sep="\t")

        pals_out_df_path = os.path.join(tmp_dir, "pals_out.csv")
        databases_dir = "/home/xvvvx/oncobox/data/pathways/"
        ONCOBOX_TOOL([
            f"--databases-dir={databases_dir}",
            f"--samples-file={pals_in_df_path}",
            f"--results-file={pals_out_df_path}"
        ])
        pals_out_df = pd.read_csv(pals_out_df_path, index_col=0)
        pals_out_df = pals_out_df.rename(columns=lambda x: {"Tumor": sample_id}.get(x, x))
        return pals_out_df.filter([sample_id])


if __name__ == "__main__":
    print(get_tcga_fields("TCGA-OR-A5JF-01A-11R-A29S-07"))
