from typing import List, Optional, Tuple

import numpy as np
import pandas as pd
import seaborn
from matplotlib import pyplot as plt
from numpy.random import choice
from tqdm import tqdm


def permutation_test(
        space: pd.Index,
        element_sets: List[pd.Index],
        n_trials: int = 10_000,
        seed: int = 4,
        plot: bool = False,
        group_name: Optional[str] = None
) -> Tuple[List, float]:
    for element_set in element_sets:
        abscent_elements = [x for x in element_set if x not in space]
        assert not abscent_elements, ",".join(abscent_elements)  # which entry elements are not in space

    entry_sizes = [len(element_set) for element_set in element_sets]

    random_intersection_sizes = list()
    np.random.seed(seed)
    for _ in tqdm(range(n_trials), total=n_trials, desc=f"Permutation test ({group_name})"):
        random_intersection = set.intersection(*[
            set(choice(space, entry_size))
            for entry_size in entry_sizes
        ])
        random_intersection_sizes.append(len(random_intersection))

    # calc p-value
    actual_intersection = list(set.intersection(*[set(x) for x in element_sets]))

    pvalue = \
        len([x for x in random_intersection_sizes if x >= len(actual_intersection)]) / len(random_intersection_sizes)

    # plot
    if plot:
        assert group_name
        ax = seaborn.histplot(random_intersection_sizes, bins=50)
        ax.set_xlabel("Cross")
        ax.set_ylabel("Frequency")
        actual_common_genes = ax.axvline(
            len(actual_intersection), color="red",
            label=f"Actual number of common entities\n"
                  f"(p-value={'<0.0001' if pvalue < .000_1 else round(pvalue, 4)})"
        )
        ax.title.set_text(f"Histogram of permutation test. ({group_name})")
        plt.legend(handles=[actual_common_genes], loc="upper right")
        plt.show()

    return actual_intersection, pvalue
